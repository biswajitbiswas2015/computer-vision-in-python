

from __future__ import  print_function

import numpy as np
import cv2 as cv
from os import system
#system('clear')
import os
#os.environ.get('TERM', '')

def main():
    try:
        img = cv.imread('Image_33.jpg')
        Z = img.reshape((-1,3))
        # Convert to float
        Z = np.float32(Z)
        # Number of clusters(K)
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        K = 3
        ret,label,center=cv.kmeans(Z,K,None,criteria,10,cv.KMEANS_RANDOM_CENTERS)
        # Convert back into uint8
        center = np.uint8(center)
        res = center[label.flatten()]
        res2 = res.reshape((img.shape))
        cv.imshow('res2',res2)
        cv.waitKey(0)
        cv.destroyAllWindows()

    except:
        pass


if __name__ == "__main__":
    main()