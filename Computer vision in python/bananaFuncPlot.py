
import numpy as np
import scipy.signal as sg
import matplotlib.pyplot as plt



def main():

    rosenbrock = lambda x, y: (1 - x) ** 2 + 100 * (y - x ** 2) ** 2
    X, Y = np.meshgrid(np.linspace(-.5, 2., 100), np.linspace(-1.5, 4., 100))
    Z = rosenbrock(X, Y)
    fig = plt.figure(figsize=(6,4), dpi= 200)
    ax = fig.gca()
    plt.contour(X, Y, Z, np.logspace(-0.5, 3.5, 20, base=10), cmap='cool')
    plt.title('Rosenbrock Function: '.format())
    plt.xlabel('x')
    plt.ylabel('y')
    plt.grid()
    plt.show()
    plt.close(fig)


if __name__ == "__main__" :
    main()


